package com.zidan.farid.newsreader.main;

import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.zidan.farid.newsreader.R;
import com.zidan.farid.newsreader.news.NewsFragment;
import com.zidan.farid.newsreader.util.BaseFragment;
import com.zidan.farid.newsreader.util.UrlWebViewFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity
    implements UrlWebViewFragment.ActivityCallback {

    @BindView(R.id.appBarLayout)
    protected AppBarLayout mAppBarLayout;
    @BindView(R.id.toolbar)
    protected Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment fragment = fragmentManager.findFragmentById(R.id.content);
        if (fragment == null) {
            fragment = NewsFragment.newInstance();
            String tag = fragment.getClass().getCanonicalName();
            fragmentManager.beginTransaction()
                    .add(R.id.content, fragment, tag)
                    .commit();
        }

        fragmentManager.addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                syncActionBarState();
            }
        });
        syncActionBarState();
    }

    private void syncActionBarState() {
        ActionBar actionBar = getSupportActionBar();
        assert actionBar != null;
        FragmentManager fragmentManager = getSupportFragmentManager();
        int backCount = fragmentManager.getBackStackEntryCount();

        if (backCount == 0) {
            actionBar.setTitle(R.string.app_name);
        } else {
            FragmentManager.BackStackEntry backStackEntry = fragmentManager.getBackStackEntryAt(backCount - 1);
            actionBar.setTitle(backStackEntry.getName());
        }
        actionBar.setDisplayHomeAsUpEnabled(backCount > 0);
        if (backCount > 0) {
            actionBar.hide();
        } else {
            actionBar.show();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {
            case R.id.action_refresh:
                NewsFragment fragment = getNewsFragment();
                if (fragment != null) {
                    fragment.refresh();
                }
                return true;
            case android.R.id.home:
                popBackStackImmediate();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment fragment = fragmentManager.findFragmentById(R.id.content);
        boolean handled = false;
        if (fragment != null && fragment instanceof BaseFragment) {
            handled = ((BaseFragment) fragment).onBackPressed();
        }
        if (!handled) {
            super.onBackPressed();
        }
    }

    private NewsFragment getNewsFragment() {
        final FragmentManager fragmentManager = getSupportFragmentManager();
        String tag = NewsFragment.class.getCanonicalName();
        Fragment fragment = fragmentManager.findFragmentByTag(tag);
        return fragment == null ? null : (NewsFragment) fragment;
    }

    protected void popBackStackImmediate() {
        final FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.popBackStack();
        fragmentManager.executePendingTransactions();
    }

    public void push(Fragment fragment, String title) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        String tag = fragment.getClass().getCanonicalName();
        fragmentManager.beginTransaction()
                //.setCustomAnimations(R.anim.in_right, R.anim.out_left, R.anim.in_left, R.anim.out_right)
                .replace(R.id.content, fragment, tag)
                .addToBackStack(title)
                .commit();
        if (title != null) {
            ActionBar actionBar = getSupportActionBar();
            if (actionBar != null) {
                actionBar.setTitle(title);
            }
        }
    }

    @Override
    public void onPageFinished() {
        // nothing
    }
}
