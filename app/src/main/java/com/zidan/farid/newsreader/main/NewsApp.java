package com.zidan.farid.newsreader.main;

import android.app.Application;

import com.zidan.farid.newsreader.BuildConfig;
import com.zidan.farid.newsreader.service.module.AppModule;
import com.zidan.farid.newsreader.service.module.DaggerNetComponent;
import com.zidan.farid.newsreader.service.module.NetComponent;
import com.zidan.farid.newsreader.service.module.NewsModule;

import timber.log.Timber;

/**
 * Created by farid on 9/29/2016.
 */

public class NewsApp extends Application {
    private static NetComponent mNetComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }

        // Dagger%COMPONENT_NAME%
        mNetComponent = DaggerNetComponent.builder()
                // list of modules that are part of this component need to be created here too
                .appModule(new AppModule(this)) // This also corresponds to the name of your module: %component_name%Module
                .newsModule(new NewsModule())
                .build();

        // If a Dagger 2 component does not have any constructor arguments for any of its modules,
        // then we can use .create() as a shortcut instead:
        //  mNetComponent = com.codepath.dagger.components.DaggerNetComponent.create();
    }

    public static NetComponent getNetComponent() {
        return mNetComponent;
    }
}
