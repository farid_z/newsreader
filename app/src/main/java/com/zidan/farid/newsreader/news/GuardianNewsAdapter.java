package com.zidan.farid.newsreader.news;

import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.zidan.farid.newsreader.R;
import com.zidan.farid.newsreader.service.model.GuardianNewsSearchResult;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by farid on 9/29/2016.
 */

public class GuardianNewsAdapter extends RecyclerView.Adapter<GuardianNewsAdapter.ViewHolder>{
    protected List<GuardianNewsSearchResult> mResults;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.newsItemTitle)
        public TextView mTitleTv;

        public ViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.news_list_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final GuardianNewsSearchResult result = getItem(position);
        holder.mTitleTv.setText(Html.fromHtml(result.webTitle == null ? "" : result.webTitle));
    }

    protected GuardianNewsSearchResult getItem(int position) {
        return mResults.get(position);
    }

    @Override
    public int getItemCount() {
        return mResults == null ? 0 : mResults.size();
    }

    public void setData(final List<GuardianNewsSearchResult> results) {
        mResults = results;
        notifyDataSetChanged();
    }
}
