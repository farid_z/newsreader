package com.zidan.farid.newsreader.news;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.zidan.farid.newsreader.R;
import com.zidan.farid.newsreader.main.MainActivity;
import com.zidan.farid.newsreader.main.NewsApp;
import com.zidan.farid.newsreader.service.api.GuardianNews;
import com.zidan.farid.newsreader.service.model.GuardianNewsSearchResp;
import com.zidan.farid.newsreader.service.model.GuardianNewsSearchResponse;
import com.zidan.farid.newsreader.service.model.GuardianNewsSearchResult;
import com.zidan.farid.newsreader.util.RecyclerItemClickListener;
import com.zidan.farid.newsreader.util.UrlWebViewFragment;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * A placeholder fragment containing a simple view.
 */
public class NewsFragment extends Fragment {
    protected final int PAGE_SIZE = 20;

    @Inject
    GuardianNews guardianNews;
    protected GuardianNewsAdapter mAdapter;

    protected Integer mPage;
    protected Integer mPages;
    protected List<GuardianNewsSearchResult> mNewsResults;
    protected Subscription mSub;

    @BindView(R.id.newsListSwipe)
    protected SwipeRefreshLayout mSwipeLayout;
    @BindView(R.id.newsRecyclerView)
    protected RecyclerView mRecyclerView;

    public static NewsFragment newInstance() {
        NewsFragment fragment = new NewsFragment();
        return fragment;
    }

    public NewsFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        NewsApp.getNetComponent().inject(this);
        mAdapter = new GuardianNewsAdapter();
        getHeadlines(1);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.news_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
//        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        final LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setAdapter(mAdapter);

        mSwipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getHeadlines(1);
            }
        });

        mRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(),
                new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                openItem(position);
            }
        }));

        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0 && mNewsResults != null && mPage != null && mPages != null && mPage < mPages
                    && layoutManager.findLastVisibleItemPosition() >= mNewsResults.size() - 2
                    && mSub != null && mSub.isUnsubscribed()) {
                    getHeadlines(mPage + 1);
                    Toast.makeText(getActivity(), R.string.loading_more, Toast.LENGTH_SHORT).show();
                }
            }

//            @Override
//            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
//                super.onScrollStateChanged(recyclerView, newState);
//                if (newState == RecyclerView.SCROLL_STATE_SETTLING) {
//                }
//            }
        });
        if (mSub != null && !mSub.isUnsubscribed()) {
            setBusy(true);
        }
    }

    public void refresh() {
        getHeadlines(1);
    }

    protected void openItem(final int position) {
        final GuardianNewsSearchResult result = mNewsResults.get(position);
        if (result.webUrl == null) {
            return;
        }
        UrlWebViewFragment fragment = UrlWebViewFragment.newInstance(result.webUrl);
        ((MainActivity) getActivity()).push(fragment, result.webTitle);
    }

    protected void setBusy(final boolean busy) {
        if (getView() != null && mSwipeLayout != null) {
            mSwipeLayout.post(new Runnable() {
                @Override
                public void run() {
                    mSwipeLayout.setRefreshing(busy);
                }
            });
        }
    }

    protected void getHeadlines(final int page) {
        setBusy(true);
        mSub = guardianNews.getHeadlines(page, PAGE_SIZE)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(new Subscriber<GuardianNewsSearchResp>() {
                @Override
                public void onCompleted() {

                }

                @Override
                public void onError(Throwable e) {
                    setBusy(false);
                }

                @Override
                public void onNext(GuardianNewsSearchResp resp) {
                    setBusy(false);
                    final GuardianNewsSearchResponse response = resp.response;
                    if (response !=null && response.results != null) {
                        mPage = page;
                        if (page == 1) {
                            mPages = response.pages;
                            mNewsResults = new ArrayList<GuardianNewsSearchResult>(resp.response.results);
                        } else {
                            mNewsResults.addAll(resp.response.results);
                        }
                        mAdapter.setData(mNewsResults);
                    }
                }
            });
    }
}
