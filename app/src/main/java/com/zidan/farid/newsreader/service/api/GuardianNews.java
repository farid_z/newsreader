package com.zidan.farid.newsreader.service.api;

import com.zidan.farid.newsreader.service.model.GuardianNewsSearchResp;
import com.zidan.farid.newsreader.service.model.GuardianNewsSearchResponse;

import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by farid on 9/29/2016.
 */

public interface GuardianNews {
    @GET("/search")
    Observable<GuardianNewsSearchResp> getHeadlines(@Query("page") int page,
                                                    @Query("page-size") int pageSize);
}
