package com.zidan.farid.newsreader.service.model;

import java.io.Serializable;
import java.util.List;

/**
 * Created by farid on 9/29/2016.
 */

public class GuardianNewsSearchResponse {
    public String status;
    public String userTier;
    public Long total;
    public Long startIndex;
    public Long pageSize;
    public Long currentPage;
    public Integer pages;
    public String orderBy;
    public List<GuardianNewsSearchResult> results;

    public String getUserTier() { return userTier; }
}
