package com.zidan.farid.newsreader.service.model;

/**
 * Created by farid on 9/29/2016.
 */

public class GuardianNewsSearchResult {
    public String id;
    public String apiUrl;
    public Boolean isHosted;
    public String sectionId;
    public String sectionName;
    public String type;
    public String webPublicationDate;
    public String webTitle;
    public String webUrl;
}
