package com.zidan.farid.newsreader.service.module;


import com.zidan.farid.newsreader.news.NewsFragment;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by farid on 9/29/2016.
 */

@Singleton
@Component(modules={AppModule.class, NewsModule.class})
public interface NetComponent {
//    void inject(MainActivity activity);
    void inject(NewsFragment fragment);
}
