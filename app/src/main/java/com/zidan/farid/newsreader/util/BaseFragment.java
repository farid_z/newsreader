package com.zidan.farid.newsreader.util;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

/**
 * Created by farid on 9/30/2016.
 */

public class BaseFragment extends Fragment{

    public boolean onBackPressed() {
        return false;
    }
}
