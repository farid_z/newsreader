package com.zidan.farid.newsreader.util;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.zidan.farid.newsreader.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by farid on 9/30/2016.
 */

public class UrlWebViewFragment extends WebViewFragment {

    static public final String ARG_URL = "Url";
    protected String url;

    public interface ActivityCallback {
        void onPageFinished();
    }

    @BindView(R.id.swipeLayout)
    protected SwipeRefreshLayout mSwipeLayout;

    public static UrlWebViewFragment newInstance(final String url) {
        final Bundle args = new Bundle();
        args.putString(ARG_URL, url);
        final UrlWebViewFragment fragment = new UrlWebViewFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        url = getArguments().getString(ARG_URL);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        final ViewGroup view = (ViewGroup) inflater.inflate(R.layout.util_urlwebview_fragment, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        final WebView webView = getWebView();
        mSwipeLayout.addView(webView, 0);
        mSwipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                webView.reload();
            }
        });

        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                setBusy(false);
                if (getActivity() != null) {
                    ((ActivityCallback) getActivity()).onPageFinished();
                }
            }
        });
        setBusy(true);
        webView.loadUrl(url);
    }

    protected void setBusy(final boolean busy) {
        if (getView() != null && mSwipeLayout != null) {
            mSwipeLayout.post(new Runnable() {
                @Override
                public void run() {
                    mSwipeLayout.setRefreshing(busy);
                }
            });
        }
    }

}
